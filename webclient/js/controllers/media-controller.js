app.controller('mediaController', [ '$scope', '$resource','$http',
		function($scope, $resource, $http) {
			var Media = $resource('/api/media');
			$scope.media = [];
			$scope.selectedMovie;
			$scope.selectedImage;

			Media.query(function(results) {
				$scope.media = results;
			});
			
			$scope.createMedia = function(entry) {
				var media = new Media();
				media.mediaTitle = $scope.mediaTitle;
				media.mediaUrl = $scope.mediaUrl;
				media.mediaPoster = $scope.mediaPoster;
				//media.entry = entry;

				media.$save(function(result) {
					$scope.media.push(result);
					$scope.mediaName = '';
					
				});
			}
			
			$scope.GetAllMovies = function () {
				$http.get('https://demo2697834.mockable.io/movies')
		            .success(function (data, status, headers, config) {
		                $scope.Movies = data;
		                console.log($scope.Movies)
		                
			            // setting default video and the image on video player
		                $scope.selectedMovie = $scope.Movies.entries[0].contents[0].url;
		                $scope.selectedImage = $scope.Movies.entries[0].images[0].url;
		            })
		            .error(function (data, status, header, config) {
		                $scope.ResponseDetails = "Data: " + data +
		                    "<br />status: " + status +
		                    "<br />headers: " + jsonFilter(header) +
		                    "<br />config: " + jsonFilter(config);
		         });
		      };
		      
		      $scope.GetAllMovies();
		      
		      $scope.playSelectedMovieFromHistory = function (media){
		    	  var videoElement = document.getElementsByTagName('video')[0];
		    	  
		    	  document.getElementById("myNav").style.width = "100%";
		    	  videoElement.poster = media.mediaPoster;
		    	  videoElement.currentSrc = media.mediaUrl;
		      }  
		      
		      $scope.changeMovieSelection = function(entry) {
		    	  $scope.mediaTitle = entry.title;
		    	  $scope.mediaUrl = entry.contents[0].url;
				  $scope.mediaPoster = entry.images[0].url;
				  
				  var videoElement = document.getElementsByTagName('video')[0];
				  
		    	  //Store the watch history to mongodb
		    	  $scope.createMedia(entry);
		    	  document.getElementById("myNav").style.width = "100%";
		    	  $scope.selectedMovie = entry.contents[0].url;
		    	  
		    	  videoElement.poster = entry.images[0].url;
		    	  videoElement.currentSrc = $scope.selectedMovie;
		      };
		      
		      $scope.closeNav =  function() {
		    	    document.getElementById("myNav").style.width = "0%";
		    	    var videoElement = document.getElementsByTagName('video')[0];
			    	  videoElement.pause();
			    	  videoElement.currentTime = 0;
		    }
		}]);