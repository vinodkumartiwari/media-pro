app.factory('mediaService', [ '$http', '$q' ],
		function mediaService($http, $q) {
			// interface
			var service = {
				movies : [],
				getMovies : getMovies
			};
			return service;

			function getMovies() {
				var def = $q.defer();

				$http.get("https://demo2697834.mockable.io/movies").success(function(data) {
					service.movies = data;
					def.resolve(data);
				}).error(function() {
					def.reject("Failed to get movies");
				});
				
				return def.promise;
			}
		});