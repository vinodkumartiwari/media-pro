#Media App VOD (MEAN Architecture)
Application that keeps track of a list of videos the user has watched. By using the API for a video list https://demo2697834.mockable.io/movies.

**Source Code**
git clone https://vinodkumartiwari@bitbucket.org/vinodkumartiwari/media-pro.git

#Build & development
Open console and navigate to the project directory 

Run **npm install** for dependencies
Run **node server** start a local development web-server Open http://localhost:3000/app/

##Step 1: Browse to the Media App VOD

npm install
npm start
http://localhost:3000/

#Create Database in MongoDB
name: media-library

##media-type
video/mp4

##Mongo Database Common Commands
database name: **media-library**
collection: **media**

1. use media-library
2. db // media-library
5. show dbs // available in the list
6. db.media.find();
7. db.media.remove({})