var Media = require('../models/media');

module.exports.create = function(request, response) {
	var media = new Media(request.body);
	media.save(function(err, result) {
		response.json(result);
	});
}

module.exports.list = function(request, response) {
	Media.find({}, function(err, results) {
		response.json(results);
	});
}
