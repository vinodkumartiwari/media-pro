var mongoose = require('mongoose');

module.exports = mongoose.model('Media', {
	mediaTitle: String,
	mediaUrl: String,
	mediaPoster: String
});