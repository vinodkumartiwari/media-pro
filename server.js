var express = require('express'),
app			= express(),
bodyParser = require('body-parser'),
mongoose = require('mongoose'),
mediaController  = require('./server/controllers/media-controller');

// create a mongo database with the name of 'media-library' and a collection with name 'media'
mongoose.connect('mongodb://localhost:27017/media-library');

app.use(bodyParser());

app.get('/', function (req, res) {
	res.sendfile(__dirname + '/webclient/views/index.html');
});

app.use('/js', express.static(__dirname + '/webclient/js'));

app.get('/api/media', mediaController.list);
app.post('/api/media', mediaController.create);

app.listen(3000, function() {
	console.log('I am listning on http://localhost:3000');
})